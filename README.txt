INTRODUCTION
------------

Optionally send an email to Group Members when new Group Content is created
or updated.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/group_notify

  * To submit bug reports or feature suggestions, or track changes:
    https://www.drupal.org/project/issues/group_notify


REQUIREMENTS
------------

This module requires the following modules:

  * [Group] (https://www.drupal.org/project/group)


RECOMMENDED MODULES
-------------------

  * [Queue Mail] (https://www.drupal.org/project/queue_mail):
    Works best for groups with a lot of members.


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. Visit
    https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  * Configure via Administration >> Groups >> Group types >> Set available
    content

    - Plugins provided by "Group Node" (gnode) can be enabled to send email
      notifications

    - Click "Install" or "Configure" and enable "Notify group members"

